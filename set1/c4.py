#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import string
import c3

def detect_xored_tex(file_name):
	good_key=float('inf')
	good_line=None
	good_ptxt=None
	sk = None
	best_score=float('inf')

	with open(file_name, 'r') as stream:
		for line in stream:
			b_text = bytes.fromhex(line.rstrip('\n'))
			sk = c3.search_key(b_text)
			if sk==None:
				continue
			ptxt=c3.xor_dec(b_text, sk)
			frequency = c3.letter_frequency(ptxt)
			score = c3.norm_L1(frequency)
		#update the score and the good secret key
			if best_score <= score:
				continue
			best_score = score
			good_key = sk
			sk = None
			good_line=line
			good_ptxt = ptxt.decode()
	return good_line.rstrip('\n'), good_key, good_ptxt.rstrip('\n')

#==============================================================================
#                               Main                                           
#==============================================================================

if __name__=="__main__":

	aux = detect_xored_tex("text/c4.txt")
	print("Ciphertext is: ", aux[0])
	print("secret key is: ", aux[1])
	print("plaintext is:  ", aux[2])
