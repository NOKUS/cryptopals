#!/usr/bin/python3.5
# -*-coding:utf-8 -*

from c2 import xor, hex_to_bytes, bytes_to_hex

def repeat_key(b_key, n):
	m = len(b_key)
	q, r = divmod(n, m)
	b_repeated_key = b_key*(q*m)
	for i in range(r):
		b_repeated_key += b_key[i:i+1]
	return b_repeated_key

def vigenere(b_ptxt, b_key):
	b_repeated_key = repeat_key(b_key, len(b_ptxt))
	b_ctxt = xor(b_ptxt, b_repeated_key)
	return b_ctxt

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	ptxt = "Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal"
	b_ptxt = ptxt.encode()
	key = 'ICE'
	b_key = key.encode()
	b_ctxt = vigenere(b_ptxt, b_key)
	h_ctxt = bytes_to_hex(b_ctxt)
	print("Ciphertext is:\n", h_ctxt, "\n")
