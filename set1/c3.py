#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import string
from c2 import xor, hex_to_bytes, bytes_to_hex

#English letter frequency
#see https://www.apprendre-en-ligne.net/crypto/stat/anglais.html

english_frequency={97:8.167, 98:1.492, 99:2.782, 100:4.253, 101:12.702, 
102:2.228, 103:2.015, 104:6.094, 105:6.966, 106:0.153, 107:0.772, 108:4.025, 
109:2.406, 110:6.749, 111:7.507, 112:1.929, 113:0.095, 114:5.987, 115:6.327, 
116:9.056, 117:2.758, 118:0.978, 119:2.360, 120:0.150, 121:1.974, 122:0.074}

"""
english_frequency = dict()
english_frequency['a'] = 8.167;	 english_frequency['n'] = 6.749
english_frequency['b'] = 1.492;	 english_frequency['o'] = 7.507
english_frequency['c'] = 2.782;	 english_frequency['p'] = 1.929
english_frequency['d'] = 4.253;	 english_frequency['q'] = 0.095
english_frequency['e'] = 12.702; english_frequency['r'] = 5.987
english_frequency['f'] = 2.228;	 english_frequency['s'] = 6.327
english_frequency['g'] = 2.015;	 english_frequency['t'] = 9.056
english_frequency['h'] = 6.094;	 english_frequency['u'] = 2.758
english_frequency['i'] = 6.966;	 english_frequency['v'] = 0.978
english_frequency['j'] = 0.153;	 english_frequency['w'] = 2.360
english_frequency['k'] = 0.772;	 english_frequency['x'] = 0.150
english_frequency['l'] = 4.025;	 english_frequency['y'] = 1.974
english_frequency['m'] = 2.406;	 english_frequency['z'] = 0.074
"""
#Intput: b_ct ciphertext, sk secret key (integer)
#Description: decrypt b_ct by xoring all bytes of b_ct with those of b_sk
#Output: b_pt: plaintext under encrypted by b_ct
def xor_dec(b_ct, sk):
	b_pt = bytes([byte^sk for byte in b_ct])
	return b_pt

def norm_L1(frequency):
	return sum(abs(frequency[b_letter] - english_frequency[b_letter]) 
		for b_letter in string.ascii_lowercase.encode())


def letter_frequency(b_text):
	b_alphabet = string.ascii_lowercase.encode()
	frequency = dict((b_letter, b_text.count(b_letter) 
	+ b_text.count(b_letter - 32)) for b_letter in b_alphabet)
	return frequency


def search_key(b_ct):
	good_sk = None
	best_score = float('inf')
	ascii_set = set(range(128))
	b_space= " ".encode()[0]

	for sk in range(256):
		b_pt = xor_dec(b_ct, sk)

		#FIRST FILTER: find if the plaintxt is printable
		if not set(b_pt) < ascii_set:
			continue

		#SECOND FILTER: use the score of square difference frequencies
		frequency = letter_frequency(b_pt)
		score = norm_L1(frequency)

		#THIRD FILTER: find the plaintext which has a best score of word separed by the character space
		if best_score < score or not b_space in b_pt:
			continue
		best_score = score
		good_sk = sk
		good_pt = b_pt
#	print('\n sk = ', good_sk, 'score = ', best_score, 'pt = \n', good_pt.decode())
	return good_sk


#==============================================================================
#                               Main                                           
#==============================================================================

if __name__=='__main__':
	ct = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
	b_ct = hex_to_bytes(ct)
	sk = search_key(b_ct)
	b_pt = xor_dec(b_ct, sk)
	print(b_pt.decode())
