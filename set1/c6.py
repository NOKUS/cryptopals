#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import base64
from c2 import xor
from c3 import search_key
from c5 import vigenere

def hamming_weight(b_string):
	return sum("1" == b for bt in b_string for b in bin(bt)[2:])

def hamming_distance(b_string1, b_string2):
	"""Calculate the Hamming distance between two byte strings"""
	assert(len(b_string1) == len(b_string2))
	b_string_diff = xor(b_string1, b_string2)
	return hamming_weight(b_string_diff)

def find_keysize(b_ctxt):

	good_KEYSIZE = 0
	min_score = float('inf')
	n = len(b_ctxt)
	for KEYSIZE in range(2,40):
		score = sum(hamming_distance(b_ctxt[i:i+KEYSIZE],
		b_ctxt[i+KEYSIZE:i+2*KEYSIZE]) for i in range(n - 2*KEYSIZE))
		score /= KEYSIZE
		if not score < min_score:
			continue
		min_score = score
		good_KEYSIZE = KEYSIZE
	return good_KEYSIZE

def transpose_blocks(b_ctxt, KEYSIZE):

	blocks = {i:bytes() for i in range(KEYSIZE)}
	for i in range(len(b_ctxt)):
		blocks[i%KEYSIZE] += b_ctxt[i:i+1]
	return blocks

def break_vigenere(file_name):
	"""Attack against repeating key encryption or Vigenere scheme"""
	b_ctxt = bytes()
	#read file and convert base64 string to bytes string
	with open(file_name, 'r') as stream:
		for line in stream:
			b_ctxt += base64.b64decode(line.rstrip("\n"))
	
	#find the key'size of secret key use for encryption
	KEYSIZE = find_keysize(b_ctxt)

	#split ciphertext in KEYSIZE blocks of ciphertexts under same key
	blocks = transpose_blocks(b_ctxt, KEYSIZE)

	#recover the key which encrypt each blocks and deduce the secret key
	b_KEY = bytes()
	for i in range(KEYSIZE):
		key = search_key(blocks[i])
		b_KEY += bytes([key])
	b_ptxt = vigenere(b_ctxt, b_KEY)
	return b_KEY, b_ptxt


#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	acc = break_vigenere("text/c6.txt")
	KEY = acc[0].decode()
	ptxt = acc[1].decode()
	print("Plaintext is: \n", ptxt)
	print("\nSecret key is: \n", KEY)
	print()
