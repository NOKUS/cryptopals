#!/usr/bin/python3.5
# -*-coding:utf-8 -*

from Crypto.Cipher import AES
import base64

def aes_decrypt(file_name, key):

	b_ctxt = bytes()
	cipher = AES.new(key, AES.MODE_ECB)
	with open(file_name, 'r') as stream:
		for line in stream:
			b_ctxt += base64.b64decode(line.rstrip('\n'))
	b_ptxt = cipher.decrypt(b_ctxt)
	# strip out padding
	nb_pad = b_ptxt[-1]
	b_ptxt = b_ptxt[:-nb_pad]
	return b_ptxt


#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	file_name = 'text/c7.txt'
	key = 'YELLOW SUBMARINE'
	b_ptxt = aes_decrypt(file_name, key)
	ptxt = b_ptxt.decode()
	print(ptxt)

