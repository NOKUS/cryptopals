#!/usr/bin/python3.5
# -*-coding:utf-8 -*


def is_ecb(b_ctxt):
	n = len(b_ctxt)

	for i in range(0, n-16, 16):
		b_c = b_ctxt[i:i+16]
		j = i+16
		while j <= n-16 and not(b_c == b_ctxt[j:j+16]):
			j += 16
		if j < n:
			return True
	return False

def detect_ecb_ciphertext(file_name):

	nb_line = 0
	with open(file_name, 'r') as stream:
		for line in stream:
			nb_line += 1
			b_ctxt = bytes.fromhex(line.rstrip('\n'))
			if is_ecb(b_ctxt):
				return nb_line, line

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	file_name = "text/c8.txt"
	nb_line, h_ctxt = detect_ecb_ciphertext(file_name)
	print('Line N°: ', nb_line)
	print("Ciphertext is: ", h_ctxt)
