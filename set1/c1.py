#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import base64
#convert from hex to bytes
def hex_to_bytes(str_hex):
	b_hex = bytes.fromhex(str_hex)
	return b_hex

#convert from bytes to hex 
def bytes_to_hex(b_hex):
	str_hex = b_hex.hex()
	return str_hex

#Input is a hex string represented in bytes ob and it output a byte ob
def hex_to_base64(b_hex):
	#decode objet bytes in string with utf-8 encoding
	#string = bytes_ob.decode('utf-8')
	b_base64 = base64.b64encode(b_hex)
	return b_base64

#Input is a base64 string represented in bytes ob and it output a byte ob
def base64_to_hex(b_base64):
	b_hex = base64.b64decode(b_base64).hex()
	return b_hex


if __name__ =='__main__':

	str_hex = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'

	b_hex = hex_to_bytes(str_hex)
	b_base64 = hex_to_base64(b_hex)
	str_base64 = b_base64.decode()
	print(str_base64)
