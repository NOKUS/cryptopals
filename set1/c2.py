#!/usr/bin/python3.5
# -*-coding:utf-8 -*
from c1 import hex_to_bytes, bytes_to_hex

#Name: function xor
#Inputs: pair of buffers (in bytes representation)
#Output: buffer (in bytes representation) which represent xor of two 
#	 inputs buffers 

def xor(b_buffer_1, b_buffer_2):
	b_buffer_3 =[b_1^b_2 for b_1, b_2 in zip(b_buffer_1, b_buffer_2)]
	return bytes(b_buffer_3)


if __name__=='__main__':
	buffer_1 = '1c0111001f010100061a024b53535009181c'
	buffer_2 = '686974207468652062756c6c277320657965'
	b_buffer_1 = hex_to_bytes(buffer_1)
	b_buffer_2 = hex_to_bytes(buffer_2)
	b_buffer_3 = xor(b_buffer_1, b_buffer_2)
	buffer_3 = bytes_to_hex(b_buffer_3)
	print(buffer_3)
