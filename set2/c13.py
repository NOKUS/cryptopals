#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import json
from collections import OrderedDict
from random import randrange
from Crypto.Cipher import AES

def pad(b_input, nb_bytes):
	try:
		assert(1 < nb_bytes <= 256)
		lth_pad = nb_bytes-(len(b_input)%nb_bytes)
		for i in range(lth_pad):
			b_input += bytes([lth_pad])
		return b_input	
	except AssertionError:
		print("Oops! number of bytes should range between 1 and 256")

def unpad(b_input):
	return b_input[:-b_input[len(b_input)-1]]
	

def random_bytes(bytes_num):
	return bytes([randrange(2**8) for i in range(bytes_num)])

AES_key = random_bytes(16)


def parse(b_url):
	key_value_pairs = OrderedDict((keyval[0].decode(), keyval[1].decode()) 
	for splited in b_url.split(b'&') for keyval in [splited.split(b'=')])
	return json.dumps(key_value_pairs, indent=2)


def profile_for(b_email):
	try:
		assert(not(b'&' in b_email or  b'=' in b_email))
		b_profile = ('email={0}&uid={1}&role={2}'.format(b_email.decode(), 10, 'usr')).encode()
		return b_profile
	except AssertionError:
		print("Oops! L'adresse email ne doit pas contenir les caractères '&' et '='")


def enc_profile(b_encoded_profile):
	encryptor  = AES.new(AES_key, AES.MODE_ECB)
	return encryptor.encrypt(pad(b_encoded_profile, 16))

def dec_profile(b_encrypted_profile):
	decryptor  = AES.new(AES_key, AES.MODE_ECB)
	return unpad(decryptor.decrypt(b_encrypted_profile))

def creat_admin_profile(b_email):
	#As structure of profile is "email=...&uid=10&role=usr", 
	#we modify b_email such that 'usr' being encrypted in the single ecb block.
	#For that, we pad b_email by space character such that length of profile
	#should be a multiple of 16. lenght of "email=&uid=10&role=" is 19 the 
	# b_email's length should be in the form 16n+13

	n = len(b_email)
	res = n%16
	if res < 13:
		b_email = b' '*(13-res)+b_email
	elif res > 13:
		b_email = b' '*(13 + (16-res))+b_email

	b_good_profile = profile_for(b_email)
	b_encrypted_good_profile = enc_profile(b_good_profile)

	# Now that the last ecb encryption of encrypted_good_profile 
	#contains only encryption of 'usr', we should creat a fack 
	#profile which contain 'admin' in the last ecb encrypted block

	b_admin = pad(b'admin', 16)
	b_domain = b'@'+b_email.split(b'@')[1]

	# As length of 'email=' is 6, we should add 10 space characters 
	#to get encryption of b_admin in the second block encryption.
	b_fake_profile = profile_for(b' '*10+b_admin+b_domain)
	b_encrypted_fake_profile = enc_profile(b_fake_profile)
	b_encrypted_admin = b_encrypted_fake_profile[16:32]
	b_encrypted_admin_profile = b_encrypted_good_profile[:-16] + b_encrypted_admin

	return b_encrypted_admin_profile

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	b_email = b'foo@bar.com'
	b_encrypted_admin_profile = creat_admin_profile(b_email)
	b_admin_profile = dec_profile(b_encrypted_admin_profile)
	print(parse(b_admin_profile))


