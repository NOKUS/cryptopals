#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import base64
import sys
sys.path.append('../set1')
from c2 import xor
from c9 import pad
from Crypto.Cipher import AES

def pad_iv(b_iv, s_key):
	block_lth = len(s_key)
	if len(b_iv) != block_lth:
		for i in range(block_lth//len(b_iv)+1):
			b_iv += b_iv
		b_iv = b_iv[0:block_lth]
	return b_iv

def aes_cbc_enc(b_iv, file_name, s_key):

	block_lth = len(s_key)
	b_input = bytes()
	b_ctxt = bytes()

	ecb = AES.new(s_key, AES.MODE_ECB)

	with open(file_name, 'r') as stream:
		for line in stream:
			b_input +=base64.b64decode(line.rstrip('\n'))

		b_input_padded = pad(b_input, block_lth)
		for i in range(0, len(b_input_padded)-block_lth, block_lth):
			b_pt_xor = xor(b_input_padded[i: i+block_lth], b_iv)
			b_ctxt_i = ecb.encrypt(b_pt_xor)
			iv = b_ctxt_i
			b_ctxt += b_ctxt_i
	return b_ctxt

def aes_cbc_dec(b_iv, file_name, s_key):

	block_lth = len(s_key)
	b_input = bytes()
	b_ctxt = bytes()

	ecb = AES.new(s_key, AES.MODE_ECB)

	with open(file_name, 'r') as stream:
		for line in stream:
			b_ctxt +=base64.b64decode(line.rstrip('\n'))

		for i in range(0, len(b_ctxt)-block_lth, block_lth):
			b_ctxt_i = b_ctxt[i:i+block_lth]
			b_ptxt_i = ecb.decrypt(b_ctxt_i)
			b_pt_xor = xor(b_ptxt_i, b_iv)
			b_iv = b_ctxt_i
			b_input += b_pt_xor
	return b_input
#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	file_name = 'text/c10.txt'
	s_key = 'YELLOW SUBMARINE'
	b_iv = b'\x00\x00\x00'
	b_iv = pad_iv(b_iv, s_key)
	print(b_iv)
	b_input = aes_cbc_dec(b_iv, file_name, s_key)

	print("Encrypted CBC mode: ", b_input.decode())
