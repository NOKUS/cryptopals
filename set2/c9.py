#!/usr/bin/python3.5
# -*-coding:utf-8 -*

def pad(b_input, nb_bytes):
	assert(1 < nb_bytes <= 256)
	lth_pad = nb_bytes-(len(b_input)%nb_bytes)
	for i in range(lth_pad):
		b_input += bytes([lth_pad])
	return b_input

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	b_input = 'YELLOW SUBMARINE'.encode()
	b_padded_input = pad(b_input, 20)
	padded_input = b_padded_input.decode()
	print("Bytes padded input : ", b_padded_input)
