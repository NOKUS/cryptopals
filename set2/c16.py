#!/usr/bin/python3.5
#-*- coding:utf-8 *-

from Crypto.Cipher import AES
from random import randrange


def pad(b_input, nb_bytes):
	try:
		assert(1 < nb_bytes <= 256)

		lth_pad = nb_bytes-(len(b_input)%nb_bytes)

		for i in range(lth_pad):
			b_input += bytes([lth_pad])

		return b_input	

	except AssertionError:
		print("Oops! number of bytes should range between 1 and 256")

def unpad(b_input):
	try:
		size = b_input[-1]
		assert(len(set(b_input[-size:]))==1)
		return b_input[:-size]

	except AssertionError:
		print("Error! Invalid PKCS#7 padding.")


def random_bytes(n):
	return bytes([randrange(256) for i in range(n)])

aes_key = random_bytes(16)
b_iv_cbc = random_bytes(16)

def first_function(b_input):
	b_pre = b"comment1=cooking%20MCs;userdata="
	b_sur = b";comment2=%20like%20a%20pound%20of%20bacon"
	
	b_url = b_pre+b_input+b_sur
	b_url = b_url.replace(b'=', b'')
	b_url = b_url.replace(b';', b'')
	b_encrypted_url = AES.new(aes_key, AES.MODE_CBC, b_iv_cbc).encrypt(pad(b_url, 16))

	return b_encrypted_url


def second_function(b_encrypted_url):
	b_decrypted_url = unpad(AES.new(aes_key, AES.MODE_CBC, b_iv_cbc).decrypt(b_encrypted_url))
	print(b_decrypted_url)

	ans = False
	if b'admin=true' in b_decrypted_url.split(b';'):
		ans = True
	return ans

def embed_admin_eq_true():

	#On utilise les caractere 'x' tel que "x^1 = '='" et "x^1 = ';'" 
	#Ce qui nous donne x = '<' et x = ':'

	b_input = b'   :admin<true:'
	b_encrypted_url = first_function(b_input)
	list_of_bytes = list(b_encrypted_url)

	#Change le premier ':' en ';'
	list_of_bytes[16] = list_of_bytes[16]^1 

	#Change '<' en '='
	list_of_bytes[22] = list_of_bytes[22]^1

	#Change le deuxieme ':' en ';'
	list_of_bytes[27] = list_of_bytes[27]^1

	return bytes(list_of_bytes)


#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':
	b_input = b";admin=true;"
	b_encrypted_url = first_function(b_input)
	b_modfied_enc_url = embed_admin_eq_true()
	print(second_function(b_modfied_enc_url))
