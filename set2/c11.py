#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import sys
sys.path.append('../set1')
import random
from Crypto.Cipher import AES
from c9 import pad
from c8 import is_ecb

def aes_keygen_128():
	return bytes([random.randrange(256) for i in range(16)])

def append(b_input):
	tmp = random.randrange(5, 10)
	b_ptxt_before = bytes([random.randrange(256) for i in range(tmp)])
	b_ptxt_after = bytes([random.randrange(256) for i in range(tmp)])
	b_ptxt = b_ptxt_before + b_input + b_ptxt_after
	return pad(b_ptxt, 16)

def choose_mode(b_input, b_key):
	if random.randint(0, 1):
		b_ctxt = AES.new(b_key, AES.MODE_ECB).encrypt(b_input)
	else:
		b_iv = aes_keygen_128()
		b_ctxt = AES.new(b_key, AES.MODE_CBC, b_iv).encrypt(b_input)
	return b_ctxt


def encryption_oracle(b_input):
	b_key = aes_keygen_128()
	b_ptxt = append(b_input)
	b_ctxt = choose_mode(b_ptxt, b_key)
	return b_ctxt

def detect_mode(b_ctxt):
	if is_ecb(b_ctxt):
		print("detected mode is ECB !")
	else:
		print("detected mode is CBC !")

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

#	b_input = 'YELLOW SUBMARINE'.encode()
#	print("Random encryption : ", encryption_oracle(b_input))
	b_input = bytes([0 for i in range(2**6)])
	for i in range(10):
		b_ctxt = encryption_oracle(b_input)
		detect_mode(b_ctxt)
