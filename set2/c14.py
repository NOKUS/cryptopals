#!/usr/bin/python3.5
# -*-coding:utf-8 -*

import sys
import base64
sys.path.append('../set1')
from c8 import is_ecb
from random import randrange, randint
from Crypto.Cipher import AES

def pad(b_input, nb_bytes):
	try:
		assert(1 < nb_bytes <= 256)
		lth_pad = nb_bytes-(len(b_input)%nb_bytes)
		for i in range(lth_pad):
			b_input += bytes([lth_pad])
		return b_input	
	except AssertionError:
		print("Oops! number of bytes should range between 1 and 256")

def unpad(b_input):
	try:
		size = b_input[-1]
		assert(len(set(b_input[-size:]))==1)
		return b_input[:-size]

	except AssertionError:
		print("Error! Invalid PKCS#7 padding.")


def random_bytes(nb_bytes):
	return bytes([randrange(256) for i in range(nb_bytes)])

b_ecb_key = random_bytes(16)
rand_count = randint(0, 2**10)
b_random_prefix = random_bytes(rand_count)

def oracle(b_input):

	b64_unknown_string = 'Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK'
	b_ptxt = b_random_prefix + b_input + base64.b64decode(b64_unknown_string)
	return AES.new(b_ecb_key, AES.MODE_ECB).encrypt(pad(b_ptxt, 16))

def detect_blocksize():
	length0 = len(oracle(b'A'))
	blocksize = len(oracle(b'AA')) - length0
	i = 3
	while blocksize <= 0:
		blocksize = len(oracle(b'A'*i)) - length0
		i +=1
	return blocksize

def detect_mode(b_ctxt, blocksize):
	mode = 'cbc'
	if len(set([b_ctxt[i:i+blocksize] for i in range(0, len(b_ctxt)-blocksize, blocksize)])) < (len(b_ctxt)/blocksize):
		mode = 'ecb'
	return mode

def detect_countsize(b_ctxt, blocksize):
	start = 0
	end = blocksize
	count = 0

	while not(b_ctxt[start: end] == b_ctxt[start+blocksize:end+blocksize]):
		count += 1
		start = end
		end += blocksize
	i = 0
	boolean = True
	while boolean:
		start = 0
		end = blocksize

		b_encrypted = oracle(b'A'*i)
		lth = len(b_encrypted)

		while (lth >= end) and not(b_encrypted[start: end] == b_encrypted[start+blocksize:end+blocksize]):
			start = end
			end += blocksize

		if end <= lth:
			i -= 2*blocksize
			boolean = False
		else:
			i+=1
		
	return count*blocksize - i

def attack():
	b_ctxt128A = oracle(b'A'*128)
	blocksize = detect_blocksize()
	countsize = detect_countsize(b_ctxt128A, blocksize)

	if detect_mode(b_ctxt128A, blocksize) == 'ecb':
		b_ctxt = oracle(b'')
		b_decrypted_ptxt = b''
		i = countsize % blocksize
		start = countsize - i
		end = start + blocksize
		i += 1
		b_prefix = b'A'*(blocksize - i)
		b_prefix1 = b_prefix + b_decrypted_ptxt

		while len(b_ctxt) != (len(b_decrypted_ptxt) + countsize):
			for j in range(256):
				if oracle(b_prefix)[start:end] == oracle(b_prefix1+bytes([j]))[start:end]:
					b_decrypted_ptxt += bytes([j])
					break
			if i == blocksize:
				start += blocksize
				end += blocksize
				i =1
			else:
				i += 1
			b_prefix = b'A'*(blocksize - i)
			b_prefix1 = b_prefix + b_decrypted_ptxt

		return b_decrypted_ptxt


#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':

	b_decrypted_ptxt = attack()
	print("decrypted plaintext is: ", b_decrypted_ptxt.decode())
