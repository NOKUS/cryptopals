#!/usr/bin/python3.5
#-*- coding:utf-8 *-

def unpad_exc(b_input):
	try:
		size = b_input[-1]
		assert(len(set(b_input[-size:]))==1)
		return b_input[:-size]

	except AssertionError:
		print("Error! Invalid PKCS#7 padding.")

#==============================================================================
#                               Main                                           
#==============================================================================


if __name__=='__main__':
	b_input = b"ICE ICE BABY\x04\x04\x04\x04"
	b_unpadded_input = unpad_exc(b_input)
	print(b_unpadded_input)
	b_input = b"ICE ICE BABY\x01\x02\x03\x04"
	b_unpadded_input = unpad_exc(b_input)
	print(b_unpadded_input)
